const request = require('supertest');
const app = require('../app');

describe('App', function() {
    it('should return a properly formatted string', function(done) {
        request(app)
            .get('/')
            .expect(200, /([A-Za-z]+) Rubber Duck/, done);
    });
}); 
