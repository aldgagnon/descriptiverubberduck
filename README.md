### DescriptiveRubberDuck

A simple web service that returns a random adjective with the string 'Rubber Duck' appended. Born out of the depths of Tribalwars.

## Installing

1. Clone the repository 
   - `$ git clone https://gitlab.com/aldgagnon/descriptiverubberduck.git`
2. Start the Docker container
   - `$ docker-compose up --build -d`