FROM node:alpine

# Create app directory
WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV


# Install app dependencies
COPY package*.json /usr/src/app/
RUN npm install

# Copy node application source
COPY . /usr/src/app

ENV PORT 5000
EXPOSE $PORT

# Default to a login shell
CMD ["npm", "start"]
