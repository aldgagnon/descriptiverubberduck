const express = require('express');
const fs = require('fs');
const random = require('random-js')();

const router = express.Router();

/**
 * Read list of adjectives
 */
const adjectives = fs.readFileSync('data/adjectives').toString().split('\n');

/* GET function */
router.get('/', (req, res) => {
  const index = random.integer(0, adjectives.length - 1);
  const returnString = `${adjectives[index]} Rubber Duck`;
  res.send(returnString);
});

module.exports = router;
